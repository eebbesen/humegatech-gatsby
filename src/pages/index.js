import React from 'react'
import { Link } from 'gatsby'

import Layout from '../components/layout'

const IndexPage = () => (
  <Layout>
    <div style={{ margin: `3rem auto`}}>
      <div className="tagline">
        Your vision realized
      </div>
      <p>Let us turn your idea into a digital experience</p>
      <hr/>
      <h3>Web Creations</h3>
      <p>Rails, JavaScript, Gatsby</p>
      <ul>
        <li><a href="https://www.ineedagyro.com">I Need A Gyro</a> (requires browser location permission)</li>
        <li><a href="https://harrison-fjord.herokuapp.com/">Harrison Fjord</a> -- careful, it may hurt your eyes</li>
        <li><a href="http://humegatech.com/">This site you are looking at right now</a></li>
      </ul>
      <h3>Alexa Skills</h3>
      <p>You must sign in to your Amazon account to view these skill pages</p>
      <ul>
        <li><a href="https://skills-store.amazon.com/deeplink/dp/B0796HFBLN?deviceType=app&share&refSuffix=ss_copy">I Need A Gyro</a></li>
        <li><a href="https://skills-store.amazon.com/deeplink/dp/B079KXCY69?deviceType=app&share&refSuffix=ss_copy">Snow Emergency</a></li>
      </ul>
      <h3>Et Cetera</h3>
      <ul>
        <li><a href="https://github.com/eebbesen/nephridium">Nephridium</a></li>
        <li><a href="https://github.com/eebbesen/freerider_api">Freerider</a></li>
        <li><a href="https://github.com/eebbesen/clickitat">Clickitat</a></li>
        <li><a href="https://github.com/eebbesen/caruby2go">Caruby2Go</a></li>
        <li><a href="https://github.com/eebbesen/car2gosling">Car2Gosling</a></li>
      </ul>

      <hr/>
      <footer>
        <a href="mailto:eric@humegatech.com">Contact us!</a>
      </footer>
    </div>
  </Layout>
)

export default IndexPage
