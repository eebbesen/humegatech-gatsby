import Typography from "typography"
import bootstrapTheme from "typography-theme-bootstrap"
import lincolnTheme from "typography-theme-lincoln"

const typography = new Typography(bootstrapTheme)

export default typography